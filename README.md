# Instagram Messenger Server

Server for Instagram messenger

## Running the server

 - You need an env var (windows) `set SECRET=yoursecret` or (*nix)`export SECRET=yoursecret`. The SECRET var is used to encrypted your password in `sha256` encryption.
- `$ npm start` to start the server.

## Notes

**N.B  Some parts of this api returns list as objects with integer keys. So when you see integer keys as { 0: {...} }, know this is part of a list** 
e.g 
```
    //This object
    var obj = {
        data : [ {...}, ,{...}, ,{...}]
    }
    
    //will be returned as
    var obj = {
        data : {
            0: { ... },
            1: { ... },
            2: { ... }
        }
    }

    //To loop through the data list..
    var length = Object.keys(obj.data).length;
    for(var i = 0; i < length; i++){
        var d = obj.data[i];
    }
```

Also, firebase database requires authentication. When /login is called, if the user is not registered on firebase, the user is created with `diaplayName: username, password: password`.  
So to call firebase directly from the web app, remember to authenticate your firebase requests.


## Api
Base url for all endpoints is: `instagram-messenger`

| endpoint   | type |     description      |  body | params | query |  result |
|:----------|:------|:-------|:------|:------- |:------ |:------|
/login | POST | logins in the user. If the user is not on firebase, create a user with `{displayName: username, password: passwoord}` | `{usernaem: ig_username, password: ig_password}` |  null | null | See login example below
/thread | GET | gets the thread items for feed | null | null | `username, id, token` | list of threads for the feed |
/event | POST | adds or updates an event to the google calendar | `{uid, description, igFullName, igFeedId, datetime}` |  | null | see example below
/event | DELETE | delete an event | `{igFeedId, uid, eventId}` | null | null | see example below
/event | PUT | update an event | `{uid, description, igFullName, igFeedId, datetime, eventId}` | 
## Examples

- Login  

    The googleauuthurl is used to authenticate teh user to google calender.
    You have to open this url on the client for the user to authetnicate.  
    This can be done immeidately after login or when the user wants to create a googel calendar event for the first time.  
    On subsequent requests when the user has already authenticated google calender, you'll not recive googleauthurl.  

    *POST*: http://localhost:8080/instagram-messenger/login  
    body: `{usernaem: njafred, password: ****}`
    Respones:  
    ```
    {
    "user": {
        "uid": "ZBQEr2btNYcOMNevs7kKB00A1WC2",
        "emailVerified": false,
        "displayName": "njafred",
        "disabled": false,
        "metadata": {
            "lastSignInTime": null,
            "creationTime": "Sun, 11 Mar 2018 12:28:29 GMT"
        },
        "passwordHash": "szDIbdlLpnK5kCZjMZy22WUHA8vcwoAuSALLLIOhLtkGCRk1lTBqCMLWc2o-HIakUGne7I6eFRdtiSBRzV6ZWQ==",
        "passwordSalt": "6zePz_6sh0IhlA==",
        "tokensValidAfterTime": "Sun, 11 Mar 2018 12:28:29 GMT",
        "providerData": []
    },
    "googleauthurl": "https://accounts.google.com/o/oauth2/v2/auth?access_type=offline&scope=https%3A%2F%2Fwww.googleapis.com%2Fauth%2Fcalendar&state=ZBQEr2btNYcOMNevs7kKB00A1WC2&prompt=consent&response_type=code&client_id=887887103975-pfa7ujh7seebtotdi1f1c23l5g5m6kat.apps.googleusercontent.com&redirect_uri=https%3A%2F%2Finstagram-messenger.herokuapp.com%2Fgoogleapiauth",
    "token": "de33c13cd4263794001d5b887a16c3979027c119259fd4a747e86a8c08fdc4dc"
}
    ```

- Get thread items for feed  
    *GET*: http://localhost:8080/instagram-messenger/thread?username=njafred&id=340282366841713400949128168202152336620&token=de33c13cd4263794001d5b887a16c3979027c119259jg4a747e86a8c08fdc4dc
    ```
    [
        {
            "itemId": "27888177378102030058048181875245056",
            "userId": 512384932,
            "timestamp": "1511821124999966",
            "itemType": "text",
            "text": "The thing is I never use the app. Im always with the  browser on my laptop. And the browser does not have these features",
            "clientContext": "7b38a6eb-43f1-4c38-b585-0c0d43b7ecf6",
            "id": "27888177378102030058048181875245056",
            "type": "text",
            "accountId": 512384932,
            "created": 1511821124999
        },
        {
            "itemId": "27888176604130420984170259410845696",
            "userId": 512384932,
            "timestamp": "1511821083042881",
            "itemType": "text",
            "text": "Haha",
            "clientContext": "1e3ee30c-237b-4d17-8e85-491926313834",
            "id": "27888176604130420984170259410845696",
            "type": "text",
            "accountId": 512384932,
            "created": 1511821083042
        }
    ]
    ```

 - *It is worth noting that some thread items can be Media* In which case you will preview the media.

    ```
    "items": {
        "0": {
            "accountId": 5819855388,
            "created": 1515611369019,
            "id": "27958095039514217918310973351919616",
            "itemId": "27958095039514217918310973351919616",
            "itemType": "media_share",
            "mediaShare": {
                "can_viewer_reshare": true,
                "can_viewer_save": true,
                "caption_is_edited": false,
                "client_cache_key": "MTY4NjExOTU2Mzg1ODM4MzI5Nw==.2",
                "code": "BdmTM9UnZHB",
                "device_timestamp": 1515220770401,
                "filter_type": 112,
                "id": "1686119563858383297_6741879533",
                "image_versions2": {
                    "candidates": {
                        "0": {
                            "height": 600,
                            "url": "https://scontent-lht6-1.cdninstagram.com/vp/4c714f2508fdeefbaf62c8fcfd454111/5B385C46/t51.2885-15/e35/26068319_137876266892074_787596847744024576_n.jpg?ig_cache_key=MTY4NjExOTU2Mzg1ODM4MzI5Nw%3D%3D.2",
                            "width": 480
                        },
                        "1": {
                            "height": 300,
                            "url": "https://scontent-lht6-1.cdninstagram.com/vp/dc32d51438667ba2ec376618e38e7a07/5B3438D4/t51.2885-15/e35/p240x240/26068319_137876266892074_787596847744024576_n.jpg?ig_cache_key=MTY4NjExOTU2Mzg1ODM4MzI5Nw%3D%3D.2",
                            "width": 240
                        }
                    }
                },
                "lat": 5.6873722931851,
                "lng": 12.737423902788,
                "location": {
                    "address": "",
                    "city": "",
                    "external_source": "facebook_places",
                    "facebook_places_id": 391430734372278,
                    "lat": 5.6873722931851,
                    "lng": 12.737423902788,
                    "location": "[Circular ~.feeds.1.items.0.mediaShare.location]",
                    "name": "Africa/Douala",
                    "pk": 391430734372278,
                    "short_name": "Africa/Douala",
                    "title": "Africa/Douala"
                },
                "media_type": 1,
                "organic_tracking_token": "eyJ2ZXJzaW9uIjo1LCJwYXlsb2FkIjp7ImlzX2FuYWx5dGljc190cmFja2VkIjp0cnVlLCJ1dWlkIjoiMjg5ZjhmNWFjOGM0NGQzMWI5YmZjOWY5ZWU1ZmU4ZTYxNjg2MTE5NTYzODU4MzgzMjk3Iiwic2VydmVyX3Rva2VuIjoiMTUyMDg1NDA2OTQ4NnwxNjg2MTE5NTYzODU4MzgzMjk3fDUxMjM4NDkzMnw4ZmJkZTlhMTRkYTQ1NTA5ZmM4MmJkNWM1ZmE5ZmY0ZDNjOTgxODQyOWEwMDZkMTM5MTMxM2Y2YzA4YzY2OGQzIn0sInNpZ25hdHVyZSI6IiJ9",
                "original_height": 600,
                "original_width": 480,
                "photo_of_you": false,
                "pk": "1686119563858383297",
                "taken_at": 1515221137,
                "user": {
                    "friendship_status": {
                        "following": false,
                        "is_bestie": false,
                        "outgoing_request": false
                    },
                    "full_name": "The Legends Gospel Accapella",
                    "has_anonymous_profile_picture": false,
                    "is_favorite": false,
                    "is_private": false,
                    "is_unpublished": false,
                    "pk": 6741879533,
                    "profile_pic_id": "1733296147210284334_6741879533",
                    "profile_pic_url": "https://scontent-lht6-1.cdninstagram.com/vp/2ab46b7afb1c504ce669b38fb281bfef/5B309573/t51.2885-19/s150x150/28753876_591379461220166_1016945532959981568_n.jpg",
                    "username": "the_legends_accapella"
                },
                "usertags": {
                    "in": {
                        "0": {
                            "position": {
                                "0": "0.38175675000000003",
                                "1": 0.13912296
                            },
                            "user": {
                                "full_name": "Barrio Dawson",
                                "is_private": false,
                                "is_verified": false,
                                "pk": 1354521992,
                                "profile_pic_id": "1497721368710650408_1354521992",
                                "profile_pic_url": "https://scontent-lht6-1.cdninstagram.com/vp/f9654e78379de5c2af9954cb9835ab44/5B3E1538/t51.2885-19/s150x150/18013655_711443629027519_8671201781331525632_a.jpg",
                                "username": "barriodawson"
                            }
                        },
                        "1": {
                            "position": {
                                "0": "0.8261434400000001",
                                "1": 0.4907668
                            },
                            "user": {
                                "full_name": "EKANE AJEBE E. KOGE",
                                "is_private": false,
                                "is_verified": false,
                                "pk": 2266808022,
                                "profile_pic_id": "1506180470864774779_2266808022",
                                "profile_pic_url": "https://scontent-lht6-1.cdninstagram.com/vp/2575101f6422eaa745976d071cba449a/5B2EB733/t51.2885-19/s150x150/18162200_827091204114771_1748813646735081472_a.jpg",
                                "username": "ekane_koge"
                            }
                        },
                        "2": {
                            "position": {
                                "0": 0.5714657,
                                "1": 0.19946423
                            },
                            "user": {
                                "full_name": "Dario Dlv",
                                "is_private": false,
                                "is_verified": false,
                                "pk": 3133997268,
                                "profile_pic_id": "1728282640006888206_3133997268",
                                "profile_pic_url": "https://scontent-lht6-1.cdninstagram.com/vp/d70039782959304657137d23bac4b348/5B3E9F78/t51.2885-19/s150x150/28158865_416746472109008_7040212084096761856_n.jpg",
                                "username": "delvis_dario"
                            }
                        },
                        "3": {
                            "position": {
                                "0": "0.11928274500000001",
                                "1": 0.39921457
                            },
                            "user": {
                                "full_name": "Joel Berka",
                                "is_private": false,
                                "is_verified": false,
                                "pk": 5321342849,
                                "profile_pic_id": "1491849847542703733_5321342849",
                                "profile_pic_url": "https://scontent-lht6-1.cdninstagram.com/vp/4793d50a1eb2238111334b879b0ffc61/5B3ACA07/t51.2885-19/s150x150/17662231_1802509316732486_1393156213931245568_a.jpg",
                                "username": "joelberka"
                            }
                        }
                    }
                }
            },
            "timestamp": "1515611369019876",
            "type": "mediaShare",
            "userId": 5819855388
        }
    }
    ```

- Create event  
    *POST* http://localhost:8080/instagram-messenger/events
    body:  
    ```
    uid:ZBQEr2btNYcOMNevs7kKB00A1WC2  //the firebase user id
    description:A test event on Instagram Messenger
    igFullName:Tasong Alidiabi  //the fullname of the igfeed
    igFeedId:340282366841710300949128132603999476609 //the igFeed this event is affiliated to
    datetime:2018-03-12T21:03:43.651Z  //The start time of the event. This time is generated with Date.toISOString()
    ```  
      
    response  
    ```
        {
        "-L7QeyHgvgVpsVLATFyW": {
            "igEvent": {
                "igFullName": "Tasong Alidiabi",
                "igFeedId": "340282366841710300949128132603999476609",
                "googleEvent": {
                    "kind": "calendar#event",
                    "etag": "\"3041779285416000\"",
                    "id": "7thru977ndnn61jln7a5h0q03c",
                    "status": "confirmed",
                    "htmlLink": "https://www.google.com/calendar/event?eid=N3RocnU5NzduZG5uNjFqbG43YTVoMHEwM2MgYWNoYWJpbGwxMkBt",
                    "created": "2018-03-12T21:20:42.000Z",
                    "updated": "2018-03-12T21:20:42.783Z",
                    "summary": "Instagram Messenger encounter with Tasong Alidiabi",
                    "description": "A test event on Instagram Messenger",
                    "creator": {
                        "email": "achabill12@gmail.com",
                        "displayName": "Acha Bill",
                        "self": true
                    },
                    "organizer": {
                        "email": "achabill12@gmail.com",
                        "displayName": "Acha Bill",
                        "self": true
                    },
                    "start": {
                        "dateTime": "2018-03-12T22:03:43+01:00"
                    },
                    "end": {
                        "dateTime": "2018-03-12T23:03:43+01:00"
                    },
                    "iCalUID": "7thru977ndnn61jln7a5h0q03c@google.com",
                    "sequence": 0,
                    "reminders": {
                        "useDefault": false,
                        "overrides": [
                            {
                                "method": "popup",
                                "minutes": 10
                            },
                            {
                                "method": "email",
                                "minutes": 1440
                            }
                        ]
                    }
                },
                "set": true
            },
            "igFeed": {
                "accounts": [
                    {
                        "friendshipStatus": {
                            "blocking": false,
                            "following": false,
                            "incoming_request": false,
                            "is_bestie": false,
                            "is_private": false,
                            "outgoing_request": false
                        },
                        "fullName": "Tasong Alidiabi",
                        "hasAnonymousProfilePicture": false,
                        "id": 1578821692,
                        "isPrivate": false,
                        "isVerified": false,
                        "picture": "https://scontent-frt3-1.cdninstagram.com/vp/3e54c2eac61cf4a8f7596ddadfdda1cc/5B3631E2/t51.2885-19/s150x150/16585114_1845286532361287_1647021998401912832_a.jpg",
                        "pk": 1578821692,
                        "profilePicId": "1450415886804793381_1578821692",
                        "profilePicUrl": "https://scontent-frt3-1.cdninstagram.com/vp/3e54c2eac61cf4a8f7596ddadfdda1cc/5B3631E2/t51.2885-19/s150x150/16585114_1845286532361287_1647021998401912832_a.jpg",
                        "username": "tasongalidiabi"
                    }
                ],
                "id": "340282366841710300949128132603999476609",
                "items": [
                    {
                        "accountId": 1578821692,
                        "created": 1517617105699,
                        "id": "27995094350718656891114491448655872",
                        "itemId": "27995094350718656891114491448655872",
                        "itemType": "text",
                        "text": "Boy na how",
                        "timestamp": "1517617105699292",
                        "type": "text",
                        "userId": 1578821692
                    }
                ]
            },
            "label": "none"
        }
    }
    ```