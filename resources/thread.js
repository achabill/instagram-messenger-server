var express = require('express')
var router = express.Router();
var Instagram = require('../instagram/instagram');

router.get('/thread', function (req, res) {
    var id = req.query['id'];
    var username = req.query['username'];
    var token = req.query['token'];


    Instagram.handleRead(id, username, token).then(response => {
        if (response.error) {
            res.send(response);
        }
        res.send(response.data);
    })
});

module.exports = router;