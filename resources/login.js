//import { google } from '../node_modules/googleapis/build/src/index';

var express = require('express')
var router = express.Router();
var Instagram = require('../instagram/instagram');
var admin = require('firebase-admin');
var feedDao = require('../dao/feed');
var googleapiauth = require('./googleapiauth');
var sessionDao = require('../dao/session');
var feeds = require('./feed');

//get a firebase user
const getUser = function (displayName) {
    return new Promise((resolve, reject) => {
        var r = [];
        admin.auth().listUsers().then(userListResult => {
            userListResult.users.forEach(user => {
                if (user.displayName == displayName) {
                    r.push(user);
                }
            });
        }).then(function () {
            if (r.length == 0) {
                resolve(null);
            } else { resolve(r[0]) }
        });
    })
};


// define the home page route
router.post('/login', function (req, res) {
    //login to instagram
    var username = req.body['username'];
    var password = req.body['password'];

    Instagram.handleRegister(username, password).then(response => {
        if (response.error) {
            res.send(response);
            return;
        }
        var igToken = response.token;
        //var session = response.session;
        var session = {
            username: username,
            password: password
        };

        //check if user already exists. If not, create user.
        getUser(username).then(user => {
            if (!user) {
                admin.auth().createUser({
                    displayName: username,
                    password: password,
                    email: username + '@igmessenger.com'
                }).then(newUser => {
                    saveSession(newUser, session).then(s => {
                        FF(newUser, igToken).then(response => res.send(response));
                    })
                }).catch(error => {
                    res.status(404).send(error);
                })
            } else {
                saveSession(user, session).then(s => {
                    FF(user, igToken).then(response => res.send(response));
                })
            }
        });
    });
});

var saveSession = function (user, session) {
    sessionDao.initializeDao();
    return new Promise(function (resolve, reject) {
        //save this user and session pair in sessions;
        sessionDao.getSessionSnapshotByUid(user.uid).then(sess => {
            if (!sess) {
                sessionDao.add({
                    uid: user.uid,
                    session: session
                }).then(s => {
                    feeds.work();
                    resolve(s)
                }
                );
            } else {
                feeds.work();
                resolve(sess);
            }
        });
    });
}


var FF = function (user, igToken) {
    return new Promise(function (resolve, reject) {
        var uid = user.uid;
        googleapiauth.token(uid).then(token => {
            if (!token) {
                googleapiauth.authUrl(uid).then(url => {
                    resolve({
                        user: user,
                        googleauthurl: url,
                        token: igToken,
                    });
                })
            } else {
                googleapiauth.refresh(token).then(t => {
                    resolve({
                        user: user,
                        googleauthurl: null,
                        token: igToken,
                    });
                });
            }
        });
    })
}

module.exports = router