var Instagram = require('../instagram/instagram');
var sessionDao = require('../dao/session');
var feedDao = require('../dao/feed');

var sync = function (uid, igFeeds) {
    return new Promise((resolve, reject) => {
        var counter = 0;
        feedDao.initializeDao(uid);
        igFeeds.forEach((igFeed, index, array) => {
            feedDao.getFeedSnapShotById(igFeed.id).then(feed => {
                if (!feed) {
                    var x = {
                        label: 'none',
                        igEvent: {
                            set: false
                        },
                        igFeed: igFeed
                    };
                    feedDao.add(x).then(() => {
                        counter++;
                        if (counter == igFeeds.length) {
                            resolve(feedDao.get());
                        }
                    })
                } else {
                    var f = feed;
                    f[Object.keys(f)[0]].igFeed = igFeed;
                    f.igFeed = igFeed;
                    feedDao.update(Object.keys(f)[0], f).then(() => {
                        counter++;
                        if (counter == igFeeds.length) {
                            resolve(feedDao.get());
                        }
                    })
                }
            }).catch(function (e) {
            })
        })
    })
};

//sync instagram feed and firebase feed
var Feed = {
    work: function () {
        console.log('working');
        var self = this;
        sessionDao.initializeDao();
        sessionDao.get().then(sessionsSnapshot => {
            if (!sessionsSnapshot) {
                return;
            }

            f(sessionsSnapshot);

            /*async.forEachOf(sessions, null, function())

            for (const key of Object.keys(sessions)) {
                var session = sessions[key];
                var uid = session.uid;
                Instagram.handleFeeds(session.session).then(igFeeds => {
                    sync(uid, igFeeds).then(x => { console.log('x: ' + uid); return; });
                });
            }
            */
        })
    }
}
var f = function (sessionsSnapshot) {
    var sessions = sessionsSnapshot.val();
    var keys = Object.keys(sessions);
    keys.forEach(async (key) => {
        var session = sessions[key];
        var uid = session.uid;
        console.log('uid: ' + uid);
        await Instagram.handleFeeds(session.session).then(async (igFeeds) => {
            await sync(uid, igFeeds).then(x => {
                console.log('synced');
            })
        })
    });
};



module.exports = Feed;
/*
router.post('/feed', function (req, res) {
    var uid = req.body.uid;
    sessionDao.getSessionSnapshotByUid(uid).then(session => {
        Instagram.handleFeeds(session.session).then(igFeeds => {
            sync(ui, igFeeds).then(x => res.send(x));
        });
    });
}); 
*/