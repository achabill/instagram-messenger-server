var express = require('express')
var router = express.Router();
var feedDao = require('../dao/feed');
var { google } = require('googleapis');
var googleapiauth = require('./googleapiauth');
var calendar = google.calendar('v3');
var stringify = require('json-stringify-safe');



var addEvent = function (uid, igEvent) {
    return new Promise(function (resolve, reject) {
        feedDao.initializeDao(uid);
        feedDao.getFeedSnapShotById(igEvent.igFeedId).then(feed => {
            if (!feed) {
                resolve({
                    'error': 'failed to get feed snapshot'
                });
                return;
            }
            feed[Object.keys(feed)[0]].igEvent = igEvent;
            feedDao.update(Object.keys(feed)[0], feed).then(f => {
                resolve(f);
                return;
            })
        })
    })
}
var deleteEvent = function (uid, igFeedId) {
    return new Promise(function (resolve, reject) {
        feedDao.initializeDao(uid);
        feedDao.getFeedSnapShotById(igFeedId).then(feed => {
            if (!feed) {
                resolve({
                    'error': 'cannot delete event on firebase'
                });
                return;
            }
            feed[Object.keys(feed)[0]].igEvent = {
                set: false,
                message: 'dummy'
            };
            feedDao.update(Object.keys(feed)[0], feed).then(f => {
                resolve(f);
                return;
            })
        })
    })
}
router.put('/event', function (req, res) {
    var uid = req.body['uid'];
    var description = req.body['description'];
    var igFullName = req.body['igFullName'];
    var igFeedId = req.body['igFeedId'];
    var datetime = req.body['datetime'];
    var eventId = req.body['eventId'];

    //auth
    googleapiauth.token(uid).then(token => {
        if (!token) {
            googleapiauth.authUrl(uid).then(url => {
                res.send({
                    error: {
                        message: 'You are not authenticated to Google Calender',
                        googleauthurl: url
                    }
                });
                return;
            });
        } else {
            var endDateTime = new Date(datetime);
            endDateTime.setHours(endDateTime.getHours() + 1);
            googleapiauth.refresh(token).then(t => {
                var gEvent = {
                    summary: 'Instagram Messenger appointment with ' + igFullName,
                    description: description,
                    start: {
                        dateTime: new Date(datetime)
                    },
                    end: {
                        dateTime: endDateTime
                    },
                    reminders: {
                        useDefault: false,
                        overrides: [
                            { 'method': 'email', 'minutes': 24 * 60 },
                            { 'method': 'popup', 'minutes': 10 },
                        ],
                    }
                }

                calendar.events.update({
                    auth: googleapiauth.oauth2Client,
                    calendarId: 'primary',
                    eventId: eventId,
                    resource: gEvent
                }, function (err, response) {
                    if (err) {
                        res.send({
                            'error': 'failed to update event',
                            'e': err
                        });
                    } else {
                        //now build database event
                        var igEvent = {
                            igFullName: igFullName,
                            igFeedId: igFeedId,
                            googleEvent: response.data,
                            set: true
                        };
                        addEvent(uid, igEvent).then(f => {
                            res.send(JSON.parse(stringify(f)));
                            return;
                        })
                    }
                });
            });
        }
    });


})
//add event
router.post('/event', function (req, res) {
    var uid = req.body['uid'];
    var description = req.body['description'];
    var igFullName = req.body['igFullName'];
    var igFeedId = req.body['igFeedId'];
    var datetime = req.body['datetime'];

    //auth
    googleapiauth.token(uid).then(token => {
        if (!token) {
            googleapiauth.authUrl(uid).then(url => {
                res.send({
                    error: {
                        message: 'You are not authenticated to Google Calender',
                        googleauthurl: url
                    }
                });
                return;
            });
        } else {
            var endDateTime = new Date(datetime);
            endDateTime.setHours(endDateTime.getHours() + 1);
            googleapiauth.refresh(token).then(t => {
                var gEvent = {
                    summary: 'Instagram Messenger encounter with ' + igFullName,
                    description: description,
                    start: {
                        dateTime: new Date(datetime)
                    },
                    end: {
                        dateTime: endDateTime
                    },
                    reminders: {
                        useDefault: false,
                        overrides: [
                            { 'method': 'email', 'minutes': 24 * 60 },
                            { 'method': 'popup', 'minutes': 10 },
                        ],
                    }
                }

                calendar.events.insert({
                    auth: googleapiauth.oauth2Client,
                    calendarId: 'primary',
                    resource: gEvent
                }, function (err, response) {
                    if (err) {
                        res.send(JSON.parse(stringify(err)).errors);
                        return;
                    }

                    //now build database event
                    var igEvent = {
                        igFullName: igFullName,
                        igFeedId: igFeedId,
                        googleEvent: response.data,
                        set: true
                    };
                    addEvent(uid, igEvent).then(f => {
                        res.send(JSON.parse(stringify(f)));
                        return;
                    })
                });
            });
        }
    });
});

router.delete('/event', function (req, res) {
    var igFeedId = req.body['igFeedId'];
    var uid = req.body['uid'];
    var eventId = req.body['eventId'];

    calendar.events.delete({
        auth: googleapiauth.oauth2Client,
        calendarId: 'primary',
        eventId: eventId
    }, function (err, response) {
        if (err) {
            res.send({
                'error': 'failed to delete event',
                'e': err
            });
            return;
        } else {
            deleteEvent(uid, igFeedId).then(f => {
                res.send(f);
            })
            return;
        }
    })
})

module.exports = router;