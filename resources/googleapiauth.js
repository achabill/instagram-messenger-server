//import { OAuth2Client } from '../node_modules/google-auth-library/build/src/auth/oauth2client';

var express = require('express')
var admin = require('firebase-admin');
var authDao = require('../dao/auth');
var { google } = require('googleapis');
var OAuth2 = google.auth.OAuth2;

authDao.initializeDao();
var authResource = {
    oauth2Client: new OAuth2(
        "887887103975-pfa7ujh7seebtotdi1f1c23l5g5m6kat.apps.googleusercontent.com",
        "C0M8npkkVZI9mKP2C5OrkZba",
        "https://instagram-messenger.herokuapp.com/googleapiauth"
    ),
    token: function (uid) {
        return new Promise(function (resolve, reject) {
            resolve(authDao.getTokenSnapshotByUid(uid))
        })
    },
    authUrl: function (uid) {
        var self = this;
        return new Promise(function (resolve, reject) {
            // generate a url that asks permissions for Google+ and Google Calendar scopes
            var scopes = [
                'https://www.googleapis.com/auth/calendar'
            ];

            var url = self.oauth2Client.generateAuthUrl({
                // 'online' (default) or 'offline' (gets refresh_token)
                access_type: 'offline',

                // If you only need one scope you can pass it as a string
                scope: scopes,

                // Optional property that passes state parameters to redirect URI
                state: uid,

                prompt: 'consent'

                //approval_prompt: 'force'
            });
            resolve(url);
        })
    },
    refresh: function (token) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.oauth2Client.setCredentials(token[Object.keys(token)[0]].token);
            self.oauth2Client.refreshAccessToken(function (err, tokens) {
                // your access_token is now refreshed and stored in oauth2Client
                // store these new tokens in a safe place (e.g. database)
                token.token = tokens;
                authDao.update(Object.keys(token)[0], token);
                resolve(token);
            });
        })
    },
    router: function () {
        var self = this;
        var r = express.Router();
        r.get('/googleapiauth', function (req, res) {

            var code = req.query['code'];
            var uid = req.query['state'];
            self.oauth2Client.getToken(code, function (err, tokens) {
                // Now tokens contains an access_token and an optional refresh_token. Save them.
                if (!err) {
                    oauth2Client.setCredentials(tokens);
                    //save tokens
                    authDao.add({
                        uid: uid,
                        token: tokens
                    }).then(function () {
                        res.send("authencicated");
                    })
                } else {
                    res.send("sorry, could not authenciate");
                }
            });
        });
        return r;
    }
}

// set auth as a global default
google.options({
    auth: authResource.oauth2Client
});

module.exports = authResource;