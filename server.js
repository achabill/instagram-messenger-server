var express = require('express');
var app = express();
var bodyParser = require('body-parser');
require('babel-register')({
    presets: ['es2015']
});
//configure firebase admin
var admin = require("firebase-admin");

var serviceAccount = require("./instagram-messenger-2c153-firebase-adminsdk-6ctkt-7ac29e79e8.json");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://instagram-messenger-2c153.firebaseio.com"
});

//configure app to use bodyparaser to get POST data from requests
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// Add headers
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();
});

//resources
var login = require('./resources/login');
var thread = require('./resources/thread');
var googleapiauth = require('./resources/googleapiauth');
var event = require('./resources/event');

//routes
var base_api = '/instagram-messenger';
app.use(base_api, login);
app.use(base_api, thread);
app.use(base_api, event);
app.use("", googleapiauth.router());

//workers
var Feed = require('./resources/feed');
setInterval(Feed.work, 160000);

//port
var port = process.env.PORT || 8899;

//secret
var secret = process.env.SECRET;
if (!secret) {
    process.env.SECRET = "secret";
}

//start the server
app.listen(port);
console.log('Listening on port: ' + port);
