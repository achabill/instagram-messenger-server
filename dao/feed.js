'use strict'
var admin = require('firebase-admin');


var feedDao = {
    db: null,
    ref: null,
    initializeDao: function (uid) {
        var self = this;
        self.db = admin.database();
        self.ref = self.db.ref().child("feeds/" + uid);
    },
    add: function (feed) {
        var self = this;
        return new Promise(function (resolve, reject) {
            var r = self.ref.push(feed);
            if (r) {
                resolve(r);
            } else {
                reject({
                    'error': 'firebase push failed',
                    'message': ''
                });
            }
        });
    },
    update: function (key, newFeed) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.ref.child(key).set(newFeed[key], function (err) {
                if (err) {
                    reject({
                        'error': err,
                        'message': 'firebase update failed'
                    });
                } else {
                    resolve(newFeed);
                }
            });
        });
    },
    delete: function (key) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.ref.child(key).set(null, function (err) {
                if (err) {
                    reject({
                        'error': err,
                        'message': ' failed to delete'
                    });
                } else {
                    resolve("deleted!");
                }
            });
        });
    },
    get: function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.ref.on("value", function (snapshot) {
                resolve(snapshot);
            }, function (err) {
                reject({
                    'error': err,
                    'message': 'firebae read failed'
                });
            });
        });
    },
    getOne: function (key) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.ref.child(key).on("value", function (snapshot) {
                resolve(snapshot.val());
            }, function (err) {
                reject({
                    'error': err,
                    'message': 'firebase read one failed'
                });
            });
        });
    },
    getFeedSnapShotById: function (id) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.get().then(feedsSnapshot => {
                var feeds = feedsSnapshot.val();
                if (!feeds) {
                    resolve(null);
                } else {
                    var r = [];
                    for (const key of Object.keys(feeds)) {
                        if (id == feeds[key].igFeed.id) {
                            var _r = {};
                            _r[key] = feeds[key];
                            r.push(_r);
                        }
                    }
                    resolve(r[0]);
                }
            })
        })
    }
}

module.exports = feedDao;
