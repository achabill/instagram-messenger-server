'use strict'
var admin = require('firebase-admin');

var sessionDao = {
    db: null,
    ref: null,
    initializeDao: function () {
        var self = this;
        self.db = admin.database();
        self.ref = self.db.ref().child("sessions");
    },
    add: function (session) {
        var self = this;
        return new Promise(function (resolve, reject) {
            var r = self.ref.push(session);
            if (r) {
                resolve(r);
            } else {
                reject({
                    'error': 'firebase push failed',
                    'message': ''
                });
            }
        });
    },
    update: function (key, session) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.ref.child(key).set(session[key], function (err) {
                if (err) {
                    reject({
                        'error': err,
                        'message': 'firebase update failed'
                    });
                } else {
                    resolve(session);
                }
            });
        });
    },
    delete: function (key) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.ref.child(key).set(null, function (err) {
                if (err) {
                    reject({
                        'error': err,
                        'message': ' failed to delete'
                    });
                } else {
                    resolve("deleted!");
                }
            });
        });
    },
    get: function () {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.ref.on("value", function (snapshot) {
                resolve(snapshot);
            }, function (err) {
                reject({
                    'error': err,
                    'message': 'firebae read failed'
                });
            });
        });
    },
    getOne: function (key) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.ref.child(key).on("value", function (snapshot) {
                resolve(snapshot.val());
            }, function (err) {
                reject({
                    'error': err,
                    'message': 'firebase read one failed'
                });
            });
        });
    },
    getSessionSnapshotByUid: function (uid) {
        var self = this;
        return new Promise(function (resolve, reject) {
            self.get().then(sessionSnapshot => {
                if (!sessionSnapshot) {
                    resolve(null);
                    return;
                }
                var sessions = sessionSnapshot.val();
                if (!sessions) {
                    resolve(null);
                    return;
                } else {
                    var r = [];
                    for (const key of Object.keys(sessions)) {
                        if (uid == sessions[key].uid) {
                            var _r = {};
                            _r[key] = sessions[key];
                            r.push(_r);
                        }
                    }
                    if (r.length == 0) {
                        resolve(null);
                    }
                    resolve(r[r.length - 1]);
                }
            })
        })
    }
}

module.exports = sessionDao;
