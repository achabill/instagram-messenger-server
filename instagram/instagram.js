import path from 'path';
import fs from 'fs';
import crypto from 'crypto';
import micro, { json } from 'micro';
import { router, get, post } from 'microrouter';
var stringify = require('json-stringify-safe');

import { V1 as Client } from 'instagram-private-api';

import { register, read, feeds } from './instapi';

const { CookieNotValidError, AuthenticationError } = Client.Exceptions;

const cors = require('micro-cors')();

const compose = (...fns) => fns.reduce((f, g) => (...args) => f(g(...args)));

const cookiesJar = path.join(__dirname, '/cookies');
if (!fs.existsSync(cookiesJar)) {
  fs.mkdirSync(cookiesJar);
}

const paramError = {
  error: {
    name: 'ParameterError',
    message: 'Both user and password are required.',
  },
};

var Instagram = {
  removeCookie: (user, key) => {
    const file = path.join(__dirname, `/cookies/${user}-${key}.json`);
    if (fs.existsSync(file)) {
      fs.unlinkSync(file);
      return true;
    }
    return false;
  },

  // Registers a client session and returns inbox feeds
  handleRegister: async (user, pass) => {
    if (!user || !pass) return paramError;

    let reg = {};
    try {
      reg = await register(user, pass);
    } catch (e) {
      if (e instanceof AuthenticationError) {
        // Remove
        const hash = crypto.createHmac('sha256', process.env.SECRET).update(pass).digest('hex');
        //self.removeCookie(user, hash);
      }
      return {
        error: {
          e: e,
          name: e.name || e,
          message: e.message || e,
        },
      };
    }
    return reg;
  },

  //handle get feeds
  handleFeeds: async (session) => {
    if (session == null) {
      return {
        error: 'session is required'
      }
    }

    let feed = {};
    try {
      feed = await feeds(session);
    } catch (e) {
      return {
        error: {
          e: e,
          name: e.name || e,
          message: e.message || e,
        },
      };
    }

    return JSON.parse(stringify(feed));
  },

  // Reads a particular feed
  handleRead: async (id, user, token) => {
    if (!id) return paramError;

    let threadItems = [];
    let params;

    if (!user || !token) return paramError;

    try {
      threadItems = await read(user, token, id);
    } catch (e) {
      if (e instanceof CookieNotValidError || e instanceof AuthenticationError) {
        // Our data is stale
        self.removeCookie(user, token);
      }
      return {
        error: {
          name: e.name || e,
          message: e.message || e,
        },
      };
    }

    return { data: threadItems };
  },

  // Erase auth
  handleLogout: async (req) => {
    let params;

    try {
      params = await json(req);
    } catch (e) {
      return paramError;
    }
    const { user, token } = params;
    if (!user || !token) return paramError;
    const isFound = self.removeCookie(user, token);
    if (isFound) {
      return {
        data: {
          success: true,
        },
      };
    }

    // Not sure it opens to bruteforce
    return {
      error: {
        name: 'LogoutError',
        message: 'This token does not exist',
      },
    };
  }
}
module.exports = Instagram;
